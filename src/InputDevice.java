public interface InputDevice {
	Direction getAction();

	boolean hasAction();
}
