import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;

public class Display
{
	private static final int CHAR_WIDTH = 32;
	private static final int CHAR_HEIGHT = 24;

	private static final int TILE_SIZE = 8;

	private static final int PIX_WIDTH = 256;
	private static final int PIX_HEIGHT = 192;

	private static final int parallaxRate = 4;

	private Game game;

	private SpecImage background;
	private SpecImage foreground;

	public Display(Game game)
	{
		this.game = game;
	}

	/**
	 * Draws the screen buffer to the window.
	 * This method must not modify the Display object.
	 * @param g The Graphics2D object to draw to.
	 */
	public void draw(Graphics2D g)
	{
		// This method should not pass the graphics object to other methods,
		// but should only do the drawing itself. This is to ensure strict
		// adherence to the Spectrum screen layout.

		Vec2i playerPos = game.getPlayer().getTruncatedPixelPos();

		Vec2i deltaPos = playerPos.minus(CHAR_WIDTH / 2, CHAR_WIDTH / 2);

		Vec2i parallaxPos = new Vec2i(deltaPos.x / parallaxRate, deltaPos.y / parallaxRate);

		// clear buffer
		g.setComposite(AlphaComposite.Clear);
		g.fillRect(0, 0, PIX_WIDTH, PIX_HEIGHT);

		BufferedImage image = new BufferedImage(PIX_WIDTH, PIX_HEIGHT, BufferedImage.TYPE_INT_ARGB);
		Graphics2D imageGraphics = image.createGraphics();

		AffineTransform gTransform = g.getTransform();
		AffineTransform imageGraphicsTransform = imageGraphics.getTransform();

		{
			g.setComposite(AlphaComposite.SrcOver);

			g.translate(parallaxPos.x,parallaxPos.y);
			renderPaper(g, background);
			g.setTransform(gTransform);

			g.translate(deltaPos.x,deltaPos.y);
			renderPaper(g, foreground);
			g.setTransform(gTransform);
		}

		{
			imageGraphics.translate(parallaxPos.x, parallaxPos.y);
			renderMask(imageGraphics, background);
			renderBitmap(imageGraphics, background);
			imageGraphics.setTransform(imageGraphicsTransform);

			imageGraphics.translate(deltaPos.x, deltaPos.y);
			renderMask(imageGraphics, foreground);
			renderBitmap(imageGraphics, foreground);
			imageGraphics.setTransform(imageGraphicsTransform);
		}

		{
			imageGraphics.setComposite(AlphaComposite.SrcIn);

			imageGraphics.translate(parallaxPos.x, parallaxPos.y);
			renderInk(imageGraphics, background);
			imageGraphics.setTransform(imageGraphicsTransform);

			imageGraphics.translate(deltaPos.x, deltaPos.y);
			renderInk(imageGraphics, foreground);
			imageGraphics.setTransform(imageGraphicsTransform);
		}

		g.drawImage(image, 0, 0, null);

		imageGraphics.dispose();
	}

	public void setBackground(SpecImage background)
	{
		this.background = background;
	}

	public SpecImage getBackground()
	{
		return background;
	}

	public void setForeground(SpecImage foreground)
	{
		this.foreground = foreground;
	}

	public SpecImage getForeground()
	{
		return foreground;
	}

	private void renderMask(Graphics2D g, SpecImage image)
	{
		g.setComposite(AlphaComposite.DstOut);

		VolatileImage mask = image.getMask();

		do
		{
			int result = mask.validate(RenderWindow.getCurrentGraphicsConfiguration());
			if (result == VolatileImage.IMAGE_RESTORED)
			{
				mask = image.renderMask();
			}
			else if (result == VolatileImage.IMAGE_INCOMPATIBLE)
			{
				mask = image.recreateMask();
			}

			g.drawImage(mask, 0, 0, null);
		}
		while (mask.contentsLost());
	}

	private void renderBitmap(Graphics2D g, SpecImage image)
	{
		g.setComposite(AlphaComposite.SrcOver);

		VolatileImage bitmap = image.getBitmap();

		do
		{
			int result = bitmap.validate(RenderWindow.getCurrentGraphicsConfiguration());
			if (result == VolatileImage.IMAGE_RESTORED)
			{
				bitmap = image.renderBitmap();
			}
			else if (result == VolatileImage.IMAGE_INCOMPATIBLE)
			{
				bitmap = image.recreateBitmap();
			}

			g.drawImage(bitmap, 0, 0, null);
		}
		while (bitmap.contentsLost());
	}

	private void renderPaper(Graphics2D g, SpecImage image)
	{
		renderAttrs(g, image, false);
	}

	private void renderInk(Graphics2D g, SpecImage image)
	{
		renderAttrs(g, image, true);
	}

	private void renderAttrs(Graphics2D g, SpecImage image, boolean ink)
	{
		Attribute[] attrs = image.getAttributes();

		int width = image.getCharWidth();
		int height = image.getCharHeight();

		for (int row = 0; row != height; ++row)
		{
			for (int col = 0; col != width; ++col)
			{
				Attribute attr = attrs[width * row + col];
				if (attr != null)
				{
					Color color = (ink) ? attr.getInkColor() : attr.getPaperColor();

					g.setColor(color);
					g.fillRect(TILE_SIZE * col, TILE_SIZE * row, TILE_SIZE, TILE_SIZE);
				}
			}
		}
	}
}
