public class Game
{
	private Display display = new Display(this);

	private InputDevice inputDevice = new KeyboardInput();

	private Entity player = new Entity(new Vec2i(15, 11));

	private int timer = 0;

	public void update()
	{
		if (++timer % 4 != 0)
			return;

		Direction d = inputDevice.getAction();

		player.move(d, 8);
	}

	public int getTimer()
	{
		return timer;
	}

	public Display getDisplay()
	{
		return display;
	}

	public Entity getPlayer()
	{
		return player;
	}
}
