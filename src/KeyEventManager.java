import java.awt.*;

class KeyEventManager {
	private static KeyEventDispatcher keyEventDispatcher = null;

	public static void storeKeyEventDispatcher(KeyEventDispatcher dispatcher) {
		if (keyEventDispatcher != null)
			throw new RuntimeException("KeyEventDispatcher is already stored!");

		keyEventDispatcher = dispatcher;
	}

	public static KeyEventDispatcher retrieveKeyEventDispatcher() {
		KeyEventDispatcher dispatcher = keyEventDispatcher;
		keyEventDispatcher = null;

		return dispatcher;
	}
}
