import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

import static java.awt.RenderingHints.*;

public class RenderWindow
{
	private static final int SCALE = 3;

	private ImageManager imageManager;

	private Canvas canvas;

	private Game game;

	public RenderWindow(Game game, GraphicsConfiguration graphicsConf)
	{
		this.game = game;

		imageManager = new ImageManager("data", graphicsConf);

		JFrame frame = new JFrame(graphicsConf);

		frame.setIgnoreRepaint(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setTitle("Game");
		frame.setFocusTraversalKeysEnabled(false);

		frame.setUndecorated(true);

		Dimension canvasDimension = new Dimension(SCALE * 256, SCALE * 192);

		canvas = new Canvas(graphicsConf);
		canvas.setIgnoreRepaint(true);
		canvas.setPreferredSize(canvasDimension);
		frame.add(canvas, BorderLayout.CENTER);

		frame.setResizable(false);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		canvas.createBufferStrategy(2);
	}

	public RenderWindow(Game game)
	{
		this(game, getCurrentGraphicsConfiguration());
	}

	public void draw()
	{
		BufferStrategy strategy = canvas.getBufferStrategy();

		do
		{
			do
			{
				Graphics2D g = (Graphics2D) strategy.getDrawGraphics();

				// TODO - can this be moved somewhere so it's only done once?
				g.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_OFF);
				g.setRenderingHint(KEY_INTERPOLATION, VALUE_INTERPOLATION_NEAREST_NEIGHBOR);

				g.scale(SCALE, SCALE);

				game.getDisplay().draw(g);

				g.dispose();
			}
			while (strategy.contentsRestored());

			strategy.show();
		}
		while (strategy.contentsLost());
	}

	public static GraphicsConfiguration getCurrentGraphicsConfiguration()
	{
		GraphicsEnvironment graphicsEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();

		GraphicsDevice graphicsDevice = graphicsEnv.getDefaultScreenDevice();

		return graphicsDevice.getDefaultConfiguration();
	}
}
