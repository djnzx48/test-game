public enum Direction {
	RIGHT, DOWN, LEFT, UP, NONE;

	/**
	 * Creates a new Direction from a delta coordinate. If the coordinate does not describe a valid direction,
	 * the value NONE is returned.
	 * @param dx The delta x value.
	 * @param dy The delta y value.
	 * @return The corresponding Direction object.
	 */
	public static Direction getDirection(int dx, int dy) {
		if (dx == 1 && dy == 0)
			return RIGHT;
		else if (dx == 0 && dy == 1)
			return DOWN;
		else if (dx == -1 && dy == 0)
			return LEFT;
		else if (dx == 0 && dy == -1)
			return UP;
		else
			return NONE;
	}

	/**
	 * Rotates the given Direction in an anticlockwise direction.
	 * @param d The Direction to be rotated.
	 * @return The rotated Direction.
	 */
	public static Direction rotateLeft(Direction d) {
		switch (d) {
			case RIGHT:
				return UP;
			case DOWN:
				return RIGHT;
			case LEFT:
				return DOWN;
			case UP:
				return LEFT;
			default:
				return NONE;
		}
	}

	/**
	 * Rotates the given Direction in a clockwise direction.
	 * @param d The Direction to be rotated.
	 * @return The rotated Direction.
	 */
	public static Direction rotateRight(Direction d) {
		switch (d) {
			case RIGHT:
				return DOWN;
			case DOWN:
				return LEFT;
			case LEFT:
				return UP;
			case UP:
				return RIGHT;
			default:
				return NONE;
		}
	}

	/**
	 * Reverses the orientation of the given Direction.
	 * @param d The Direction to be reversed.
	 * @return The reversed Direction.
	 */
	public static Direction reverse(Direction d) {
		switch (d) {
			case RIGHT:
				return LEFT;
			case DOWN:
				return UP;
			case LEFT:
				return RIGHT;
			case UP:
				return DOWN;
			default:
				return NONE;
		}
	}

	/**
	 * Creates a Direction from a string value.
	 * @param string The given string.
	 * @return The corresponding Direction.
	 */
	public static Direction fromString(String string) {
		switch (string) {
			case "right":
				return RIGHT;
			case "down":
				return DOWN;
			case "left":
				return LEFT;
			case "up":
				return UP;
			case "none":
				return NONE;
			default:
				return null;
		}
	}

	/**
	 * Creates a string from a Direction value.
	 * @param dir The given direction.
	 * @return The corresponding string.
	 */
	public static String asString(Direction dir) {
		switch (dir) {
			case RIGHT:
				return "right";
			case DOWN:
				return "down";
			case LEFT:
				return "left";
			case UP:
				return "up";
			case NONE:
				return "none";
			default:
				return null;
		}
	}

	/**
	 * Returns the Direction as a coordinate value.
	 * @param dir The given direction.
	 * @return The corresponding coordinate value.
	 */
	public static Coord<Integer> getCoord(Direction dir) {
		int dx = 0;
		int dy = 0;

		switch (dir) {
			case RIGHT:
				dx = 1;
				break;
			case DOWN:
				dy = 1;
				break;
			case LEFT:
				dx = -1;
				break;
			case UP:
				dy = -1;
				break;
			default:
				return null;
		}

		return new Coord<>(dx, dy);
	}
}
