import java.awt.*;

public class Attribute
{
	private static final Color BLACK 			= new Color(0x00, 0x00, 0x00);
	private static final Color BLUE				= new Color(0x00, 0x00, 0xc0);
	private static final Color RED				= new Color(0xc0, 0x00, 0x00);
	private static final Color MAGENTA			= new Color(0xc0, 0x00, 0xc0);
	private static final Color GREEN			= new Color(0x00, 0xc0, 0x00);
	private static final Color CYAN				= new Color(0x00, 0xc0, 0xc0);
	private static final Color YELLOW			= new Color(0xc0, 0xc0, 0x00);
	private static final Color WHITE			= new Color(0xc0, 0xc0, 0xc0);

	private static final Color BRIGHT_BLACK		= new Color(0x00, 0x00, 0x00);
	private static final Color BRIGHT_BLUE		= new Color(0x00, 0x00, 0xff);
	private static final Color BRIGHT_RED		= new Color(0xff, 0x00, 0x00);
	private static final Color BRIGHT_MAGENTA	= new Color(0xff, 0x00, 0xff);
	private static final Color BRIGHT_GREEN		= new Color(0x00, 0xff, 0x00);
	private static final Color BRIGHT_CYAN		= new Color(0x00, 0xff, 0xff);
	private static final Color BRIGHT_YELLOW	= new Color(0xff, 0xff, 0x00);
	private static final Color BRIGHT_WHITE		= new Color(0xff, 0xff, 0xff);

	public static final Color[] colors =
	{
			BLACK,
			BLUE,
			RED,
			MAGENTA,
			GREEN,
			CYAN,
			YELLOW,
			WHITE
	};

	public static final Color[] brights =
	{
			BRIGHT_BLACK,
			BRIGHT_BLUE,
			BRIGHT_RED,
			BRIGHT_MAGENTA,
			BRIGHT_GREEN,
			BRIGHT_CYAN,
			BRIGHT_YELLOW,
			BRIGHT_WHITE
	};

	private int paper;
	private int ink;
	private boolean bright;

	public Attribute(int paper, int ink, boolean bright)
	{
		if (paper < 0 || paper > 7 || ink < 0 || ink > 7)
			throw new IllegalArgumentException("Invalid attribute (paper: " + paper + " ink: " + ink + ")");

		this.paper = paper;
		this.ink = ink;
		this.bright = bright;
	}

	public Color getPaperColor()
	{
		if (bright)
			return brights[paper];
		else
			return colors[paper];
	}

	public Color getInkColor()
	{
		if (bright)
			return brights[ink];
		else
			return colors[ink];
	}

	public boolean isBright()
	{
		return bright;
	}

	public void setBright(boolean bright)
	{
		this.bright = bright;
	}
}
