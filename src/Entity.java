public class Entity
{
	private Vec2i pos;

	public Entity(Vec2i pos)
	{
		this.pos = pos;
	}

	public Vec2i getPixelPos()
	{
		return pos;
	}

	public Vec2i getCharPos()
	{
		return new Vec2i(pos.x / 8, pos.y / 8);
	}

	public Vec2i getTruncatedPixelPos()
	{
		Vec2i charPos = getCharPos();

		return new Vec2i(8 * charPos.x, 8 * charPos.y);
	}

	public void move(Direction d, int step)
	{
		int x = pos.x;
		int y = pos.y;

		switch (d)
		{
			case RIGHT:
				x += step;
				break;
			case DOWN:
				y += step;
				break;
			case LEFT:
				x -= step;
				break;
			case UP:
				y -= step;
				break;
		}

		// TODO
		while (x < 0)
		{
			x += 256;
		}
		while (x >= 256)
		{
			x -= 256;
		}

		while (y < 0)
		{
			y += 192;
		}
		while (y >= 192)
		{
			y -= 192;
		}

		pos = new Vec2i(x, y);
	}
}
