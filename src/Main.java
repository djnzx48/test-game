import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicReference;

public class Main
{
	private static final long TIMESTEP = 50;
	private static final long DELTA_TIME = 1_000_000_000L / TIMESTEP; // 50 fps

	private static final long MIN_FPS = 5;
	private static final long MIN_FPS_TIME = 1_000_000_000L / MIN_FPS;

	public static void main(String[] args)
	{
		try
		{
			Game game = new Game();

			final AtomicReference<RenderWindow> render = new AtomicReference<>();

			SwingUtilities.invokeAndWait(() -> render.set(new RenderWindow(game)));

			runMainLoop(game, render.get());
		}
		catch (InterruptedException ignored)
		{
		}
		catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
	}

	private static void runMainLoop(Game game, RenderWindow render)
			throws InvocationTargetException, InterruptedException
	{
		try
		{
			BufferedImage backgroundImage = ImageIO.read(new File("data/flashback_bg2.png"));
			SpecImage background = new SpecImage(backgroundImage, true);

			BufferedImage foregroundImage = ImageIO.read(new File("data/flashback_fg2.png"));
			SpecImage foreground = new SpecImage(foregroundImage, false);

			Display display = game.getDisplay();

			display.setBackground(background);
			display.setForeground(foreground);
		}
		catch (IOException | Tile.TooManyColorsException | Tile.TooManyBrightLevelsException e)
		{
			e.printStackTrace();
		}

		long currentTime = System.nanoTime();

		while (true)
		{
			// update game and render
			game.update();
			SwingUtilities.invokeAndWait(render::draw);
			Toolkit.getDefaultToolkit().sync();

			long newTime = System.nanoTime();

			// wait for next frame
			while (newTime - currentTime < DELTA_TIME)
			{
				try
				{
					Thread.sleep(1);
				}
				catch (Exception ignored)
				{
				}

				newTime = System.nanoTime();
			}

			// update time
			if (newTime - currentTime < MIN_FPS_TIME)
			{
				currentTime += DELTA_TIME;
			}
			else
			{
				currentTime = newTime;
			}
		}
	}
}
