import java.util.Objects;

public class Coord<T> {
	public final T x;
	public final T y;

	public Coord(T x, T y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		Coord<?> coord = (Coord<?>) o;
		return x.equals(coord.x) &&
				y.equals(coord.y);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(x, y);
	}
}
