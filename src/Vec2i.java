public class Vec2i
{
	public final int x;
	public final int y;

	public Vec2i(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Vec2i(Vec2i other)
	{
		this.x = other.x;
		this.y = other.y;
	}

	public Vec2i minus(int x, int y)
	{
		return new Vec2i(this.x - x, this.y - y);
	}
}
