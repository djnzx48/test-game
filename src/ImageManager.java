import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ImageManager
{
	private Map<String, VolatileImage> images = new HashMap<>();
	private File directory;

	private GraphicsConfiguration graphicsConf;

	public ImageManager(String directory, GraphicsConfiguration graphicsConf)
	{
		this.directory = new File(directory);
		this.graphicsConf = graphicsConf;
	}

	public VolatileImage getImage(String name) throws IOException
	{
		VolatileImage image = images.get(name);

		if (image == null)
		{
			BufferedImage bufferedImage = ImageIO.read(new File(directory, name));

			image = graphicsConf.createCompatibleVolatileImage(
					bufferedImage.getWidth(), bufferedImage.getHeight(), Transparency.BITMASK);

			Graphics2D graph2D = image.createGraphics();
			try
			{
				// clear the image
				graph2D.setComposite(AlphaComposite.Src);
				graph2D.setColor(Color.BLACK);
				graph2D.clearRect(0, 0, image.getWidth(), image.getHeight());

				graph2D.drawImage(bufferedImage, null, 0, 0);
			}
			finally
			{
				graph2D.dispose();
			}

			images.put(name, image);
		}

		return image;
	}

	public void setDirectory(String directory)
	{
		// invalidate existing images
		images.clear();

		this.directory = new File(directory);
	}
}

