import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Stack;

public class KeyboardInput implements InputDevice {
	private int upKey = KeyEvent.VK_W;
	private int leftKey = KeyEvent.VK_A;
	private int downKey = KeyEvent.VK_S;
	private int rightKey = KeyEvent.VK_D;

	private final Stack<Direction> actionStack = new Stack<>();

	public KeyboardInput() {
		// we need to remove the previous KeyEventDispatcher before we add a new one
		// otherwise the old one will swallow all the key events and we won't be able to receive them
		KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();

		KeyEventDispatcher prevDispatcher = KeyEventManager.retrieveKeyEventDispatcher();
		focusManager.removeKeyEventDispatcher(prevDispatcher);

		KeyEventDispatcher dispatcher = this::processKeyEvent;
		KeyEventManager.storeKeyEventDispatcher(dispatcher);

		focusManager.addKeyEventDispatcher(dispatcher);
	}

	@Override
	public Direction getAction() {
		synchronized (actionStack) {
			if (actionStack.empty())
				return Direction.NONE;

			return actionStack.peek();
		}
	}

	@Override
	public boolean hasAction() {
		return true;
	}

	private boolean processKeyEvent(KeyEvent keyEvent) {
		int keyCode = keyEvent.getKeyCode();
		Direction action;

		if (keyCode == upKey)
			action = Direction.UP;
		else if (keyCode == leftKey)
			action = Direction.LEFT;
		else if (keyCode == downKey)
			action = Direction.DOWN;
		else if (keyCode == rightKey)
			action = Direction.RIGHT;
		else
			return false;

		int id = keyEvent.getID();

		synchronized (actionStack) {
			if (id == KeyEvent.KEY_PRESSED) {
				if (!actionStack.contains(action)) {
					actionStack.push(action);
				}

				return true;
			} else if (id == KeyEvent.KEY_RELEASED) {
				actionStack.remove(action);

				return true;
			}
		}

		return false;
	}
}
