import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.VolatileImage;

public class SpecImage
{
	private static final int TILE_SIZE = 8;

	private int charWidth;
	private int charHeight;

	private final Tile[] tiles;

	private boolean bitmapCacheValid = false;
	private VolatileImage cachedBitmap;

	private boolean maskCacheValid = false;
	private VolatileImage cachedMask;

	private boolean attrCacheValid = false;
	private Attribute[] cachedAttributes;

	public SpecImage(BufferedImage image, boolean allowBlank) throws Tile.TooManyColorsException, Tile.TooManyBrightLevelsException
	{
		int width = image.getWidth();
		int height = image.getHeight();

		if (width % TILE_SIZE != 0 || height % TILE_SIZE != 0)
		{
			throw new IllegalArgumentException("Image dimensions must be multiples of " + TILE_SIZE);
		}

		if (width == 0 || height == 0)
		{
			throw new IllegalArgumentException("Image must have nonzero dimensions");
		}

		charWidth = width / TILE_SIZE;
		charHeight = height / TILE_SIZE;

		tiles = new Tile[charWidth * charHeight];

		int[] pixels = new int[TILE_SIZE * TILE_SIZE];

		// iterate over 8x8 blocks
		for (int charRow = 0; charRow < charHeight; ++charRow)
		{
			for (int charCol = 0; charCol < charWidth; ++charCol)
			{
				int charX = TILE_SIZE * charCol;
				int charY = TILE_SIZE * charRow;

				// get pixels in block
				pixels = image.getRGB(charX, charY,
						TILE_SIZE, TILE_SIZE,
						pixels,
						0, TILE_SIZE);

				Tile tile = Tile.create(pixels, allowBlank);

				tiles[charRow * charWidth + charCol] = tile;
			}
		}
	}

	public VolatileImage getBitmap()
	{
		return (bitmapCacheValid) ? cachedBitmap : recreateBitmap();
	}

	public VolatileImage getMask()
	{
		return (maskCacheValid) ? cachedMask : recreateMask();
	}

	public Attribute[] getAttributes()
	{
		if (attrCacheValid)
		{
			return cachedAttributes;
		}

		Attribute[] attributes = new Attribute[charWidth * charHeight];

		for (int row = 0; row < charHeight; ++row)
		{
			for (int col = 0; col < charWidth; ++col)
			{
				Tile tile = tiles[row * charWidth + col];

				if (tile != null)
				{
					Attribute attr = tile.getAttribute();
					attributes[row * charWidth + col] = attr;
				}
			}
		}

		cachedAttributes = attributes;
		attrCacheValid = true;

		return cachedAttributes;
	}

	public VolatileImage renderBitmap()
	{
		return renderBitmap(cachedBitmap);
	}

	public VolatileImage renderMask()
	{
		return renderMask(cachedMask);
	}

	public VolatileImage recreateBitmap()
	{
		int width = getWidth();
		int height = getHeight();

		GraphicsConfiguration gc = RenderWindow.getCurrentGraphicsConfiguration();

		VolatileImage bitmap = gc.createCompatibleVolatileImage(width, height, Transparency.BITMASK);

		return renderBitmap(bitmap);
	}

	public VolatileImage recreateMask()
	{
		int width = getWidth();
		int height = getHeight();

		GraphicsConfiguration gc = RenderWindow.getCurrentGraphicsConfiguration();

		VolatileImage mask = gc.createCompatibleVolatileImage(width, height, Transparency.BITMASK);

		return renderMask(mask);
	}

	public int getCharWidth()
	{
		return charWidth;
	}

	public int getCharHeight()
	{
		return charHeight;
	}

	public int getWidth()
	{
		return TILE_SIZE * charWidth;
	}

	public int getHeight()
	{
		return TILE_SIZE * charHeight;
	}

	private VolatileImage renderBitmap(VolatileImage bitmap)
	{
		int width = getWidth();
		int height = getHeight();

		do
		{
			GraphicsConfiguration gc = RenderWindow.getCurrentGraphicsConfiguration();

			if (bitmap.validate(gc) == VolatileImage.IMAGE_INCOMPATIBLE)
			{
				bitmap = gc.createCompatibleVolatileImage(width, height, Transparency.BITMASK);
			}

			Graphics2D bitmapGraphics = bitmap.createGraphics();
			bitmapGraphics.setComposite(AlphaComposite.Src);

			bitmapGraphics.setColor(Tile.PIXEL_RESET_COLOR);
			bitmapGraphics.clearRect(0, 0, width, height);

			for (int row = 0; row < charHeight; ++row)
			{
				for (int col = 0; col < charWidth; ++col)
				{
					Tile tile = tiles[row * charWidth + col];

					if (tile != null)
					{
						int imageX = TILE_SIZE * col;
						int imageY = TILE_SIZE * row;

						bitmapGraphics.drawImage(tile.getBitmap(), imageX, imageY, null);
					}
				}
			}

			bitmapGraphics.dispose();
		}
		while (bitmap.contentsLost());

		cachedBitmap = bitmap;
		bitmapCacheValid = true;

		return bitmap;
	}

	private VolatileImage renderMask(VolatileImage mask)
	{
		int width = getWidth();
		int height = getHeight();

		do
		{
			GraphicsConfiguration gc = RenderWindow.getCurrentGraphicsConfiguration();

			if (mask.validate(gc) == VolatileImage.IMAGE_INCOMPATIBLE)
			{
				mask = gc.createCompatibleVolatileImage(width, height, Transparency.BITMASK);
			}

			Graphics2D maskGraphics = mask.createGraphics();
			maskGraphics.setComposite(AlphaComposite.Src);

			maskGraphics.setColor(Tile.PIXEL_RESET_COLOR);
			maskGraphics.clearRect(0, 0, width, height);

			maskGraphics.setColor(Tile.PIXEL_SET_COLOR);

			for (int row = 0; row < charHeight; ++row)
			{
				for (int col = 0; col < charWidth; ++col)
				{
					Tile tile = tiles[row * charWidth + col];

					if (tile != null)
					{
						int imageX = TILE_SIZE * col;
						int imageY = TILE_SIZE * row;

						maskGraphics.fillRect(imageX, imageY, TILE_SIZE, TILE_SIZE);
					}
				}
			}

			maskGraphics.dispose();
		}
		while (mask.contentsLost());

		cachedMask = mask;
		maskCacheValid = true;

		return mask;
	}
}
