import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;

public class Tile
{
	private static final int SIZE = 8;

	private static final IndexColorModel model;

	public static final Color PIXEL_RESET_COLOR = new Color(0, 0, 0, 0);
	public static final Color PIXEL_SET_COLOR = new Color(255, 255, 255, 255);

	private static final int COLOR_THRESHOLD = 128;
	private static final int BRIGHT_THRESHOLD = 224;

	private BufferedImage bitmap;
	private Attribute attr;

	private Tile(int[] pixels) throws TooManyColorsException, TooManyBrightLevelsException
	{
		bitmap = new BufferedImage(SIZE, SIZE, BufferedImage.TYPE_BYTE_BINARY, model);

		Boolean blockBright = null;
		int paperAttr = -1;
		int inkAttr = -1;

		for (int pixelRow = 0; pixelRow < SIZE; ++pixelRow)
		{
			for (int pixelCol = 0; pixelCol < SIZE; ++pixelCol)
			{
				int rgb = pixels[SIZE * pixelRow + pixelCol];

				int b = (rgb) & 0xff;
				int g = (rgb >> 8) & 0xff;
				int r = (rgb >> 16) & 0xff;

				Boolean pixelBright = null;

				Boolean blueBright = isBright(b);
				if (blueBright != null)
				{
					pixelBright = blueBright;
				}

				Boolean greenBright = isBright(g);
				if (greenBright != null)
				{
					if (pixelBright != null && pixelBright != greenBright)
						throw new TooManyBrightLevelsException();

					pixelBright = greenBright;
				}

				Boolean redBright = isBright(r);
				if (redBright != null)
				{
					if (pixelBright != null && pixelBright != redBright)
						throw new TooManyBrightLevelsException();

					pixelBright = redBright;
				}

				if (pixelBright != null)
				{
					if (blockBright != null && pixelBright != blockBright)
						throw new TooManyBrightLevelsException();

					blockBright = pixelBright;
				}

				boolean pixelSet;
				int pixelAttr = getPixelAttr(r, g, b);
				if (paperAttr == -1 || paperAttr == pixelAttr)
				{
					paperAttr = pixelAttr;
					pixelSet = false;
				}
				else if (inkAttr == -1 || inkAttr == pixelAttr)
				{
					inkAttr = pixelAttr;
					pixelSet = true;
				}
				else
				{
					throw new TooManyColorsException(
							"[paper: " + paperAttr
							+ " ink: " + inkAttr
							+ " pixel: " + pixelAttr + "]");
				}

				bitmap.setRGB(pixelCol, pixelRow, (pixelSet)
						? PIXEL_SET_COLOR.getRGB()
						: PIXEL_RESET_COLOR.getRGB());
			}
		}

		if (blockBright == null)
			blockBright = false;

		if (inkAttr == -1)
			inkAttr = 0;

		attr = new Attribute(paperAttr, inkAttr, blockBright);
	}

	public boolean isBlank()
	{
		// check all rgb values in bitmap are equal
		int rgb = bitmap.getRGB(0, 0);

		for (int i = 0; i < 8; ++i)
		{
			for (int j = 0; j < 8; ++j)
			{
				if (rgb != bitmap.getRGB(i, j))
					return false;
			}
		}

		return true;
	}

	public BufferedImage getBitmap()
	{
		return bitmap;
	}

	public Attribute getAttribute()
	{
		return attr;
	}

	public static Tile create(int[] pixels, boolean allowBlank) throws TooManyColorsException, TooManyBrightLevelsException
	{
		Tile tile = new Tile(pixels);

		return (!allowBlank && tile.isBlank()) ? null : tile;
	}

	private static Boolean isBright(int color)
	{
		if (color >= BRIGHT_THRESHOLD)
			return true;
		else if (color >= COLOR_THRESHOLD)
			return false;
		else
			return null;
	}

	private static int getPixelAttr(int r, int g, int b)
	{
		int attr = 0;

		// component order: grb
		if (b >= COLOR_THRESHOLD)
			attr += 1;

		if (r >= COLOR_THRESHOLD)
			attr += 2;

		if (g >= COLOR_THRESHOLD)
			attr += 4;

		return attr;
	}

	static
	{
		byte[] redComponent     = new byte[]{(byte) PIXEL_RESET_COLOR.getRed(), (byte) PIXEL_SET_COLOR.getRed()};
		byte[] greenComponent   = new byte[]{(byte) PIXEL_RESET_COLOR.getGreen(), (byte) PIXEL_SET_COLOR.getGreen()};
		byte[] blueComponent    = new byte[]{(byte) PIXEL_RESET_COLOR.getBlue(), (byte) PIXEL_SET_COLOR.getBlue()};
		byte[] alphaComponent   = new byte[]{(byte) PIXEL_RESET_COLOR.getAlpha(), (byte) PIXEL_SET_COLOR.getAlpha()};

		model = new IndexColorModel(1, 2,
				redComponent, greenComponent, blueComponent, alphaComponent);
	}

	// TODO: maybe move this to somewhere else
	public static IndexColorModel getIndexColorModel()
	{
		return model;
	}

	public static class TooManyBrightLevelsException extends Exception
	{
		public TooManyBrightLevelsException()
		{
		}

		public TooManyBrightLevelsException(String msg)
		{
			super(msg);
		}
	}

	public static class TooManyColorsException extends Exception
	{
		public TooManyColorsException()
		{
		}

		public TooManyColorsException(String msg)
		{
			super(msg);
		}
	}
}
